import stocks


class MockResponse:
    def __init__(self, data):
        self.data = data
        # self.symbol_data = symbol_data

    def json(self):
        return self.data


# def test_get_symbol_data(mocker):
#     mocker.patch('requests.get', mock_data)
#     data = stocks.symbol_data()
#     return data


def mock_symbol_data(*args):
    data = {
        "c": 319.2207,
        "h": 319.52,
        "l": 316.2,
        "o": 316.75,
        "pc": 313.14,
        "t": 1590004550,
    }
    return MockResponse(data)


def test_get_stock_price(mocker):
    def mock_get_ticker(prompt):
        return "AAPL"

    mocker.patch("requests.get", mock_symbol_data)
    symbol_data = stocks.get_stock_price(get_ticker=mock_get_ticker)

    assert symbol_data["c"] == 319.2207
    assert symbol_data["h"] == 319.52
    assert symbol_data["l"] == 316.2
    assert symbol_data["o"] == 316.75


def mock_no_data(*args):
    data = "NOTASTOCK"
    return MockResponse(data)


def test_get_stock_price_invalid(mocker):
    def mock_get_ticker(prompt):
        return "APPL"

    mocker.patch("requests.get", mock_no_data)
    symbol_data = stocks.get_stock_price(get_ticker=mock_get_ticker)

    assert symbol_data == "NOTASTOCK"


def mock_data(*args):
    data = [
        {"description": "JOINT CORP/THE", "displaySymbol": "JYNT", "symbol": "JYNT"},
        {"description": "KELLOGG CO", "displaySymbol": "K", "symbol": "K"},
    ]
    return MockResponse(data)


def test_find_symbol(mocker):
    def mock_find_stock(prompt):
        return "kello"

    mocker.patch("requests.get", mock_data)
    business, symbol = stocks.find_symbol(find_stock=mock_find_stock)

    assert business == "KELLOGG CO"
    assert symbol == "K"
