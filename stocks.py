#! /usr/bin/env python3
import requests

API_KEY = "br1ffmvrh5reisn52r40"


def symbol_data():
    data = requests.get(
        f"https://finnhub.io/api/v1/stock/symbol?exchange=US&token={API_KEY}"
    )
    raw_data = data.json()
    return raw_data


def get_stock_price(get_ticker=input):
    """Requests a stock ticker string and fetches the price of it
      Defaults to using the input to request user input"""
    SYMBOL = get_ticker("Stock symbol: ").upper()
    try:
        symbol_data = requests.get(
            f"https://finnhub.io/api/v1/quote?symbol={SYMBOL}&token={API_KEY}"
        )
        symbol_data = symbol_data.json()
    except ValueError:
        print("Ticker does not exist")  # Look here to debug
        symbol_data = "NOTASTOCK"
    else:
        print("c = current price , h = high price, l = low price, o = open")
    return symbol_data


# def user_input_company():
#     find_stock = input('What company would you like to see the symbol for? ').upper()
#     return find_stock

# Add try..except blocks
def find_symbol(find_stock=input):
    """Requests a company name to search for the tikker symbol.
       Will return a list of matches and their symbol."""
    raw_data = symbol_data()
    find_stock = find_stock(
        "What company would you like to see the symbol for? "
    ).upper()
    for company in raw_data:
        if find_stock in company["description"]:
            business = company["description"]
            symbol = company["symbol"]
            print(business, "symbol :", symbol)
    return business, symbol


# def main():
#     raw_data = get_stock_symbol()
#     find_stock = user_input_company()
#     get_symbol(find_stock, raw_data)
#     # symbol = user_input()
#     # symbol_data = get_stock_price(symbol)
#     # print(symbol_data)


# main()
